import datetime
from os import environ

from flask import Flask, request, Response, make_response, jsonify, abort
from mongoengine import errors, Document, StringField, EmailField, DateTimeField, connect

app = Flask(__name__)
connect('playvox', host='mongo_notes')


class Notes(Document):
    title = StringField(max_length=100, required=True)
    user = EmailField(max_length=100, unique_with='title', required=True)
    body = StringField(max_length=2000, required=True)
    _created_at = DateTimeField(default=datetime.datetime.utcnow)


@app.route('/v1/notes/<user>', methods=['GET'])
def get_notes(user):
    notes = Notes.objects(user=user)
    if notes.count() is 0:
        return Response(notes.to_json(), status=404, mimetype='application/json')

    return Response(notes.to_json(), status=200, mimetype='application/json')


@app.route('/v1/notes/<user>', methods=['POST'])
def add_note(user):
    note = Notes()
    note.user = user
    note.title = request.json['title']
    note.body = request.json['body']

    try:
        note.save()
    except errors.NotUniqueError:
        return Response("Duplicated note - title", status=500, mimetype='application/json')
    except Exception as e:
        return "%s" % e
    return Response(note.to_json(), status=201, mimetype='application/json')


@app.route('/v1/notes/<user>/<title>', methods=['PUT', 'PATCH'])
def update_note(user: str, title: str):
    try:
        note = Notes.objects.get(user=user, title=title)
    except errors.DoesNotExist as e:
        return "%s" % e

    if isinstance(note, Notes):
        if not request.json:
            abort(400)
        if 'body' in request.json and type(request.json['body']) != str:
            abort(400)

        if 'body' in request.json:
            note.body = request.json['body']
        try:
            note.save()
        except Exception as e:
            return "\n %s" % e
        return Response(note.to_json(), status=200, mimetype='application/json')

    else:
        abort(404)


@app.route('/v1/notes/<user>/<title>', methods=['DELETE'])
def delete_note(user: str, title: str):
    try:
        note = Notes.objects.get(user=user, title=title)
        note.delete()
    except errors.DoesNotExist as e:
        return "%s" % e
    return Response("note_deleted", status=200, mimetype='application/json')


@app.errorhandler(404)
def not_found():
    return make_response(jsonify({'error': 'User not found, please verify data'}), 404)


if __name__ == '__main__':
    port = int(environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
