from os import environ

from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
def index():
    return render_template('index.html')


if __name__ == '__main__':
    port = int(environ.get('PORT', 80))
    app.run(debug=False, host='0.0.0.0', port=port)
