# Api Playvox

## Requirements

To build this project you will need [Docker][Docker Install] and [Docker Compose][Docker Compose Install].

## Deploy and Run

After cloning this repository, you can type the following command to start the app:

```sh
make build 
```
Or type 
```sh 
make help
```
for additional commands.

Then simply visit:

- Client [localhost][App]
- User Endpoints [localhost/v1/users/][Users]
- Notes Endpoints [localhost/v1/notes/email-of-user][Notes]
- Traefik [localhost:8080][Traefik]

###User Model
- first_name
- last_name
- email
- age
- gender

###Note Model
- title
- body
- user - user email


[Docker Install]:  https://docs.docker.com/install/
[Docker Compose Install]: https://docs.docker.com/compose/install/
[App]: http://127.0.0.1
[Users]: http://127.0.0.1/v1/users/
[Notes]: http://127.0.0.1/v1/notes/emailOfUser
[Traefik]: http://127.0.0.1:8080

