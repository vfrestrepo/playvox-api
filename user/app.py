import datetime
import json
from os import environ

from flask import Flask, request, Response, make_response, jsonify, abort
from mongoengine import errors, Document, StringField, EmailField, IntField, DateTimeField, connect

app = Flask(__name__)
connect('playvox', host='mongo_users')
GENDERS = (('m', 'Male'), ('f', 'Female'))


class User(Document):
    email = EmailField(max_length=100, unique=True)
    first_name = StringField(max_length=50, required=True)
    last_name = StringField(max_length=50)
    age = IntField()
    gender = StringField(max_length=1, choices=GENDERS)
    _created_at = DateTimeField(default=datetime.datetime.utcnow)


@app.route('/v1/users/<email>/', methods=['GET'])
def get_user(email: str):
    try:
        user = User.objects.get(email=email)
    except errors.DoesNotExist as e:
        return "%s" % e

    return Response(user.to_json(), status=200, mimetype='application/json')


@app.route('/v1/users/', methods=['GET'])
def get_users():
    request_args = request.args
    limit = 10
    page = int(request_args.get('page')) if 'page' in request_args else 1
    query = request_args.get('query') if 'query' in request_args else None
    offset = 0
    if page > 1:
        offset = (page - 1) * limit

    user_list = User.objects.skip(offset).limit(limit)

    if query is not None:
        dic = json.loads(query)
        if 'first_name' in dic:
            user_list = User.objects(first_name=list(dic.values())[0]).skip(offset).limit(limit)
        if 'last_name' in dic:
            user_list = User.objects(last_name=list(dic.values())[0]).skip(offset).limit(limit)
        if 'age' in dic:
            user_list = User.objects(age=list(dic.values())[0]).skip(offset).limit(limit)
        if 'gender' in dic:
            user_list = User.objects(gender=list(dic.values())[0]).skip(offset).limit(limit)
        if 'email' in dic:
            user_list = User.objects(email=list(dic.values())[0]).skip(offset).limit(limit)

    if user_list.count() is 0:
        return Response(user_list.to_json(), status=404, mimetype='application/json')

    return Response(user_list.to_json(), status=200, mimetype='application/json')


@app.route('/v1/users/', methods=['POST'])
def add_user():
    user = User()
    user.first_name = request.json['first_name']
    user.last_name = request.json['last_name']
    user.age = request.json['age']
    user.gender = request.json['gender']
    user.email = request.json['email']

    try:
        user.save()
    except errors.NotUniqueError:
        return Response("Duplicated email", status=500, mimetype='application/json')
    except Exception as e:
        return "%s" % e
    return Response(user.to_json(), status=201, mimetype='application/json')


@app.route('/v1/users/<email>/', methods=['PUT', 'PATCH'])
def update_user(email):
    try:
        user = User.objects.get(email=email)
    except errors.DoesNotExist as e:
        return "%s" % e

    if isinstance(user, User):
        if not request.json:
            abort(400)
        if 'first_name' in request.json and type(request.json['first_name']) != str:
            abort(400)
        if 'last_name' in request.json and type(request.json['last_name']) != str:
            abort(400)
        if 'age' in request.json and type(request.json['age']) != int:
            abort(400)
        if 'gender' in request.json and type(request.json['gender']) != str:
            abort(400)

        if 'first_name' in request.json:
            user.first_name = request.json['first_name']
        if 'last_name' in request.json:
            user.last_name = request.json['last_name']
        if 'age' in request.json:
            user.age = request.json['age']
        if 'gender' in request.json:
            user.gender = request.json['gender']

        try:
            user.save()
        except Exception as e:
            return "\n %s" % e
        return Response(user.to_json(), status=200, mimetype='application/json')

    else:
        abort(404)


@app.route('/v1/users/<email>/', methods=['DELETE'])
def delete_user(email):
    try:
        user = User.objects.get(email=email)
        user.delete()
    except errors.DoesNotExist as e:
        return "%s" % e
    return Response("Deleted", status=200, mimetype='application/json')


@app.errorhandler(404)
def not_found():
    return make_response(jsonify({'error': 'User not found, please verify data'}), 404)


if __name__ == '__main__':
    port = int(environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
